<html lang="de-DE">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
  <title>Traditionssegler «LOVIS» &#8211; Ein Freiraum zur Verwirklichung von Träumen</title>

  <link rel='stylesheet' id='sage/css-css'
        href='https://lovis.de/wp-content/themes/lovis/dist/styles/main-54d1c283e7.css' type='text/css' media='all'/>
  <style>
    body {
      background-image: none;
      background-color: transparent;
    }
  </style>

</head>
<body>

<form action="https://news.lovis.de/public/listmessenger.php" method="post" accept-charset="utf-8">
  <input type="hidden" name="group_ids[]" value="2"/>

  <div class="form-group">
    <label for="email_address">E-Mail Adresse</label>
    <input type="email" class="form-control" id="email_address" name="email_address" maxlength="128"/>
    <small id="emailHelp" class="form-text text-muted">Wir geben Deiner E-Mail-Adresse niemals an andere.</small>
  </div>


  <div class="form-group">
    <label for="firstname">Vorname</label>
    <input class="form-control" id="firstname" name="firstname" value="" maxlength="32"/>
  </div>

  <div class="form-group">
    <label for="lastname">Nachname</label>
    <input class="form-control" id="lastname" name="lastname" value="" maxlength="32"/>
  </div>

  <div class="form-group">
    <img src="https://news.lovis.de/public/listmessenger.php?action=captcha" width="172" height="45"
         alt="CAPTCHA Image" title="CAPTCHA Image"/>
    <input class="form-control" id="captcha_code" name="captcha_code" value="" style="width: 170px"/>
    <label for="captcha_code" class="required">Security Code</label>

  </div>


  <div class="form-group">
    <label for="action">Aktion</label>

    <select class="form-control" id="action" name="action">
      <option value="subscribe">Eintragen</option>
      <option value="unsubscribe">Austragen</option>
    </select>
  </div>

  <input type="submit" class="btn btn-secondary" value="Abschicken"/>


</form>
</body>
</html>
