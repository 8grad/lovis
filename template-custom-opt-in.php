<?php
/**
 * Template Name: Opt-In
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

<!-- Form -->
<form id="optin-form" method="post" action="<?php echo admin_url('admin-ajax.php?action=opt_in_submit'); ?>">
  <div>
    <div id="status-box"
         style="background: #f8f9fa; padding: 10px; margin-bottom: 20px; border: solid 1px #ddd; display: none;"></div>
    <!-- Status Box -->

    <h3>Eintragen</h3>
    <input type="hidden" name="nonce"/>

    <div class="form-group" style="display: none;" aria-hidden="true">
      <label for="website">Webseite</label>
      <input type="text" class="form-control" id="website" name="website" maxlength="128"/>
      <small id="emailHelp" class="form-text text-muted"></small>
    </div>

    <div class="form-group">
      <label for="email_address">E-Mail Adresse <strong>*</strong></label>
      <input required type="email" class="form-control" id="email_address" name="email" maxlength="128"/>
      <small id="emailHelp" class="form-text text-muted">Wir geben Deiner E-Mail-Adresse niemals an andere
        weiter.</small>
    </div>

    <div class="form-group">
      <label for="lastname">Name (optional)</label>
      <input class="form-control" id="lastname" name="name" value="" maxlength="32"/>
    </div>

    <p hidden>
      <input id="f2487" type="checkbox" name="l" checked value="f24876db-be33-46d6-be8a-6fb0887f9a6e"/>
      <label for="f2487">newsletter</label>
    </p>

    <div class="checkbox">
      <label class="form-check-label" for="flexCheckDefault">
        <input class="form-check-input" type="checkbox" required name="terms" id="flexCheckDefault">
        <strong>*</strong> Ja, ich möchte den Newsletter mit Informationen zur LOVIS bestellen/abbestellen.
        Einen Hinweis zu der von der Einwilligung mit umfassenden Protokollierung der Anmeldung/Abmeldung
        und Deinen Widerrufsrechten erhältst Du in der <a href="https://lovis.de/datenschutzerklaerung/">Datenschutzerklärung</a>.
      </label>
    </div>

    <p><strong>* Pflichtfeld</strong></p>
    <p><input class="btn btn-secondary" type="submit" value="Abonnieren"/></p>
  </div>
</form>


<script>
  document.addEventListener("DOMContentLoaded", function () {
    // Select the form and the status box
    const form = document.getElementById("optin-form");
    const statusBox = document.getElementById("status-box");

    form.addEventListener("submit", function (event) {
      event.preventDefault(); // Prevent default form submission

      // Prepare the form data
      const formData = new FormData(form);

      // Perform AJAX request
      fetch(form.action, {
        method: "POST",
        body: formData,
      })
        .then((response) => response.json())
        .then((data) => {
          // Clear and display the status box
          statusBox.style.display = "block"; // Show the status box
          if (data.success) {
            statusBox.style.backgroundColor = "#d4edda"; // Light Green
            statusBox.style.color = "#155724"; // Dark Green
            statusBox.style.border = "1px solid #c3e6cb"; // Success Border
            statusBox.textContent = data.data.message; // Set success message
          } else {
            statusBox.style.backgroundColor = "#f8d7da"; // Light Red
            statusBox.style.color = "#721c24"; // Dark Red
            statusBox.style.border = "1px solid #f5c6cb"; // Error Border
            statusBox.textContent = data.data.message; // Set error message
          }
        })
        .catch((error) => {
          // Handle network or server errors
          console.error("Error submitting the form:", error);
          statusBox.style.display = "block";
          statusBox.style.backgroundColor = "#f8d7da"; // Light Red
          statusBox.style.color = "#721c24"; // Dark Red
          statusBox.style.border = "1px solid #f5c6cb"; // Error Border
          statusBox.textContent =
            "An unexpected error occurred. Please try again.";
        });
    });
  });
</script>
