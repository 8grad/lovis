# [LOVIS on base of old Sage 9](https://roots.io/sage/)


## Theme development
Sage uses [gulp](http://gulpjs.com/) as its build system and [Bower](http://bower.io/) to manage front-end packages.

### Install gulp and Bower
Building the theme requires [node.js](http://nodejs.org/download/). We recommend you update to the latest version of npm: `npm install -g npm@latest`.

From the command line:

1. Install [gulp](http://gulpjs.com) and [Bower](http://bower.io/) globally with `npm install -g gulp bower`
2. Navigate to the theme directory, then run `npm install`
3. Run `bower install`

You now have all the necessary dependencies to run the build process.

### Available gulp commands

* `gulp` Compile and optimize the files in your assets directory
* `gulp watch` Compile assets when file changes are made
* `gulp --production` Compile assets for production (no source maps).


## start development


``` shell
docker compose up -d && nvm use && npm install && gulp watch
```
