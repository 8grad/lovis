<div class="page-header">
  <h1><?= __('All Events', 'sage'); ?></h1>
</div>
<br/>
<br/>
<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no events were found.', 'sage'); ?>
  </div>
<?php endif; ?>

<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
  'post_type' => 'events',
  'post_status' => 'publish',
  'posts_per_page' => 10,
  'paged' => $paged,
  'ignore_sticky_posts' => 1,
  'orderby' => 'meta_value',
  'meta_key' => 'von',        // the custom meta_key name
  'order' => 'DESC',
);

$my_query = new WP_Query($args);

?>

<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
<?php endwhile; ?>

<?php the_posts_navigation(); ?>
