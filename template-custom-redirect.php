<?php
/**
 * Template Name: redirect to events
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

<meta http-equiv="refresh" content="0; URL=<?= esc_url(home_url('/')); ?>events/">
