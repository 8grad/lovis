<div class="page-header">
  <h1><?= __('Cruises Overview', 'sage'); ?></h1>
</div>
<br />
<br />

<?php

$args = array(
  'post_type' => 'events',
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'max_num_pages' > 0,
  'category_name' => 'toern',
  'ignore_sticky_posts' => 1,
  'orderby' => 'meta_value',
  'meta_key' => 'von',
  'order' => 'DESC',

  'meta_query' => array(  // WordPress has all the results, now, return only the events after today's date
    array(
      'key' => 'von', // Check the start date field
      'value' => date("Y+1-m-d"), // Set today's date (note the similar format)
      'compare' => '>=', // Return the ones greater than today's date
      'type' => 'DATE' // Let WordPress know we're working with date
    )
  ),
);

$my_query = null;
$my_query = new WP_Query($args);
?>

<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
<?php endwhile; ?>

<?php the_posts_navigation(); ?>
