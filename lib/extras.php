<?php

namespace Roots\Sage\Extras;

use DateTime;
use IntlDateFormatter;
use Roots\Sage\Setup;
use WP_Query;

/**
 * Add <body> classes
 */
function body_class($classes)
{
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}

add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more()
{
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}

add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');


/**
 * create custom post type for sailing trips
 */

function create_my_post_types()
{

  register_post_type('events',
    array(
      'labels' => array(
        'name' => __('Events', 'sage'),
        'singular_name' => __('events', 'sage'),
        'add_new' => __('create new event', 'sage'),
        'add_new_item' => __('create event', 'sage'),
        'edit' => __('Edit', 'sage'),
        'edit_item' => __('edit event', 'sage'),
        'new_item' => __('new event', 'sage'),
        'view' => __('view event', 'sage'),
        'view_item' => __('view event', 'sage'),
        'search_items' => __('search events', 'sage'),
        'not_found' => __('no event found', 'sage'),
        'not_found_in_trash' => __('no event in trash', 'sage'),
        'parent' => __('Parent event', 'sage'),
      ),
      'menu_icon' => 'dashicons-calendar',
      'public' => true,
      'has_archive' => true,
      'capability_type' => 'post',
      'menu_position' => 5,
      'supports' => array('title', 'custom-fields', 'editor', 'thumbnail'),
      'taxonomies' => array('category', 'post_tag'),
      'rewrite' => array(
        'slug' => 'events',
        'pages' => true,
      ),
    )
  );

  register_taxonomy_for_object_type('category', 'events');

}

add_action('init', __NAMESPACE__ . '\\create_my_post_types');


/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 *
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length($length)
{
  return 20;
}

add_filter('excerpt_length', __NAMESPACE__ . '\\wpdocs_custom_excerpt_length', 999);


/**
 * Filter the except length to 20 words.
 *
 * @param int $more .
 *
 * @return string (Maybe) modified excerpt length.
 */

function custom_excerpt_more($more)
{
  return ' … ';
}

add_filter('excerpt_more', __NAMESPACE__ . '\\custom_excerpt_more');


function checkLegacyOfDate($postId)
{

  $customDateValues = get_post_custom_values("bis", $postId);
  if (empty($customDateValues) || !isset($customDateValues[0])) {
    return null; // Return early if no custom date is found
  }

  $postDateFormatted = formatDateString($customDateValues[0]);
  $todayFormatted = (new DateTime())->format('Y-m-d');

  if (isDatePast($postDateFormatted, $todayFormatted)) {
    return "status--completed";
  }

  return null; // Explicitly returning null for clarity
}

function formatDateString($date)
{
  return date('Y-m-d', strtotime($date)); // Using date() instead of strftime() for simplicity
}

function isDatePast($postDate, $today)
{
  return $postDate <= $today;
}


/**
 * echo custom post events
 *
 * @throws \DateMalformedStringException
 */

function get_the_events()
{
  {
    $args = array(
      'post_type' => 'events',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'ignore_sticky_posts' => 1,
      'orderby' => 'meta_value',
      'meta_key' => 'von',    // the custom meta_key name
      'order' => 'ASC',
      'meta_query' => array(  // WordPress has all the results, now, return only the events after today's date
        array(
          'key' => 'bis', // Check the start date field
          'value' => date("Y-m-d"), // Set today's date (note the similar format)
          'compare' => '>=', // Return the ones greater than today's date
          'type' => 'DATE' // Let WordPress know we're working with date
        )
      ),
    );

    //$my_query = null;
    $my_query = new WP_Query($args);

    if ($my_query->have_posts()) {
      ?>
      <ol class="events__list">
        <?php
        while ($my_query->have_posts()) : $my_query->the_post();

          $theID = get_the_ID();
          $subtitle = get_post_custom_values("untertitel", $theID);

          $dateFrom = get_post_custom_values("von", $theID);
          $dateTo = get_post_custom_values("bis", $theID);

          $formatter = new IntlDateFormatter(
            'de_DE', // German locale
            IntlDateFormatter::FULL, // Date style for full format
            IntlDateFormatter::NONE // Time style (none in this case)
          );
          $formatter->setPattern('EEE, d. MMM Y'); // Custom pattern (like strftime)

          $niceDateFrom = $formatter->format(new DateTime($dateFrom[0]));
          $niceDateTo = $formatter->format(new DateTime($dateTo[0]));


          ?>
          <li class="events__item drop-shadow lifted <?php
          if (in_category(['abgesagt'])) {
            echo "events__item--canceled";
            $title = "findet nicht statt";
          } else {
            $title = "";
          }
          ?>" title="<?= $title ?>">
            <h4>
              <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title() ?></a>
            </h4>
            <h5>
              <?php
              if (!empty($subtitle) && isset($subtitle[0])) {
                echo $subtitle[0];
              } else {
                echo ''; // Fallback to an empty string if $subtitle is null or invalid.
              }

              ?>
            </h5>
            <p>
              <strong>
                <?php
                if (in_category(['abgesagt'])) {
                  echo "<del>";
                }
                ?>
                <?php echo $niceDateFrom . "&mdash;" . $niceDateTo ?>
                <?php
                if (in_category(['abgesagt'])) {
                  echo "</del>";
                }
                ?>
              </strong>
            </p>
            <p>
              <?php echo the_excerpt(); ?>
            </p>
          </li>

        <?php
        endwhile;
        ?>
      </ol>
      <?php

    } // if have posts

  }
}


// Breadcrumbs
function custom_breadcrumbs()
{

  // Settings
  $separator = '&gt;';
  $breadcrums_id = 'breadcrumbs';
  $breadcrums_class = 'breadcrumbs';
  $home_title = 'Homepage';

  // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
  $custom_taxonomy = 'product_cat';

  // Get the query & post information
  global $post, $wp_query;

  // Do not display on the homepage
  if (!is_front_page()) {

    // Build the breadcrums
    echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';

    // Home page
    echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
    echo '<li class="separator separator-home"> ' . $separator . ' </li>';

    if (is_archive() && !is_tax() && !is_category() && !is_tag()) {

      echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title("", false) . '</strong></li>';

    } else if (is_archive() && is_tax() && !is_category() && !is_tag()) {

      // If post is a custom post type
      $post_type = get_post_type();

      // If it is a custom post type display name and link
      if ($post_type != 'post') {

        $post_type_object = get_post_type_object($post_type);
        $post_type_archive = get_post_type_archive_link($post_type);

        echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
        echo '<li class="separator"> ' . $separator . ' </li>';

      }

      $custom_tax_name = get_queried_object()->name;
      echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';

    } else if (is_single()) {

      // If post is a custom post type
      $post_type = get_post_type();

      // If it is a custom post type display name and link
      if ($post_type != 'post') {

        $post_type_object = get_post_type_object($post_type);
        $post_type_archive = get_post_type_archive_link($post_type);

        echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
        echo '<li class="separator"> ' . $separator . ' </li>';

      }

      // Get post category info
      $category = get_the_category();

      if (!empty($category)) {

        // Get last category post is in
        //$last_category = end(array_values($category));
        $last_category = end($category);


        // Get parent any categories and create array
        $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','), ',');
        $cat_parents = explode(',', $get_cat_parents);

        // Loop through parent categories and store in variable $cat_display
        $cat_display = '';
        foreach ($cat_parents as $parents) {
          $cat_display .= '<li class="item-cat">' . $parents . '</li>';
          $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
        }

      }

      // If it's a custom post type within a custom taxonomy
      $taxonomy_exists = taxonomy_exists($custom_taxonomy);
      if (empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {

        $taxonomy_terms = get_the_terms($post->ID, $custom_taxonomy);
        $cat_id = $taxonomy_terms[0]->term_id;
        $cat_nicename = $taxonomy_terms[0]->slug;
        $cat_link = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
        $cat_name = $taxonomy_terms[0]->name;

      }

      // Check if the post is in a category
      if (!empty($last_category)) {
        echo $cat_display;
        echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

        // Else if post is in a custom taxonomy
      } else if (!empty($cat_id)) {

        echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
        echo '<li class="separator"> ' . $separator . ' </li>';
        echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

      } else {

        echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

      }

    } else if (is_category()) {

      // Category page
      echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';

    } else if (is_page()) {

      // Standard page
      if ($post->post_parent) {

        // If child page, get parents
        $anc = get_post_ancestors($post->ID);

        // Get parents in the right order
        $anc = array_reverse($anc);

        // Parent page loop
        if (!isset($parents)) {
          $parents = null;
        }
        foreach ($anc as $ancestor) {
          $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
          $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
        }

        // Display parent pages
        echo $parents;

        // Current page
        echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';

      } else {

        // Just display current page if not parents
        echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';

      }

    } else if (is_tag()) {

      // Tag page

      // Get tag information
      $term_id = get_query_var('tag_id');
      $taxonomy = 'post_tag';
      $args = 'include=' . $term_id;
      $terms = get_terms($taxonomy, $args);
      $get_term_id = $terms[0]->term_id;
      $get_term_slug = $terms[0]->slug;
      $get_term_name = $terms[0]->name;

      // Display the tag name
      echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';

    } elseif (is_day()) {

      // Day archive

      // Year link
      echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link(get_the_time('Y')) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
      echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';

      // Month link
      echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link(get_the_time('Y'), get_the_time('m')) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
      echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';

      // Day display
      echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';

    } else if (is_month()) {

      // Month Archive

      // Year link
      echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link(get_the_time('Y')) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
      echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';

      // Month display
      echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';

    } else if (is_year()) {

      // Display year archive
      echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';

    } else if (is_author()) {

      // Auhor archive

      // Get the author information
      global $author;
      $userdata = get_userdata($author);

      // Display author name
      echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';

    } else if (get_query_var('paged')) {

      // Paginated archives
      echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">' . __('Page') . ' ' . get_query_var('paged') . '</strong></li>';

    } else if (is_search()) {

      // Search results page
      echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';

    } elseif (is_404()) {

      // 404 page
      echo '<li>' . 'Error 404' . '</li>';
    }

    echo '</ul>';

  }

}


/* display images  */
/* first update image sizes */
add_image_size('image-header', 676, 345, array('center', 'center')); // Hard crop left top

/**
 * return $images_id array()
 */
function get_gallery_attachments()
{
  global $post;

  if ($post) {
    $post_content = $post->post_content;

    preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);

    if (isset($ids[1]) && $ids[1] != '') {
      $images_id = explode(",", $ids[1]);

      return $images_id;
    }
  }
}

/**
 * return DEFAULT $images_id array()
 */
function get_default_gallery_attachments()
{
  $post = get_post(6);

  $post_content = $post->post_content;
  preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
  $images_id = explode(",", $ids[1]);

  return $images_id;
}

function gallery_shortcode_LOVIS()
{
  $output = "";
  $attachments = get_gallery_attachments();

  if (empty($attachments) || (isset($attachments[0]) && $attachments[0] === "")) {
    $attachments = get_default_gallery_attachments();
  }

  $output .= "<div class='image-header--image_container swiper-container'>";
  $output .= "<ul class='image-header--inner_images swiper-wrapper'>";

  foreach ($attachments as $key) {
    $imgLarge = wp_get_attachment_image_src($key, 'image-header');
    //$imgSrc = wp_get_attachment_image_src( $key, "full" );
    $imgIcon = wp_get_attachment_image_src($key, "medium");

    if ($imgLarge) {
      $output .= "<li class=\"swiper-slide\" data-hash=\"image-$key\">";
      $output .= "<img class='image-header--inner_image isLoading' alt='' width='" . $imgLarge[1] . "' height='" . $imgLarge[2] . "' data-src=\"$imgLarge[0]\" src='" . $imgIcon[0] . "' />";
      $output .= "<!-- Preloader image -->";
      $output .= "<div class=\"swiper-lazy-preloader swiper-lazy-preloader-black\"></div>";

      $output .= "<!-- Copyright image -->";
      if (isset($photo) && $photo != '') {
        $output .= "<div class='swiper-photo-credit'>" . __('Photo © ', 'sage') . " $photo[0] </div>";
      }
    }
  }

  $output .= "</ul>";

  /* picture overlapping pola */
  $output .= "<div class='image-header--vignette'></div>";

  $output .= "</ul>
        <!-- Add Scrollbar -->
        <div class=\"swiper-scrollbar\"></div>

        <!-- Add Pagination -->
        <ul class=\"swiper-pagination\"></ul>

        <!-- Add Arrows -->
        <div class=\"swiper-button-next swiper-button-black\"></div>
        <div class=\"swiper-button-prev swiper-button-black\"></div>";


  $output .= "</div>";

  return $output;
}

/**
 * Remove shortcodes if we are on the home page
 *
 * @param string $content Post content.
 *
 * @return string (Maybe) modified post content.
 */
function wpdocs_remove_shortcode_from_index($content)
{
  $content = strip_shortcodes($content);

  return $content;
}


/**
 * Get Subpages of 'Aktionen'
 *
 *
 */


function projectsOnLovis()
{
  $args = array(
    'post_type' => 'page',
    'post_status' => 'publish',
    'post_parent' => 60,
    'posts_per_page' => -1,
    'ignore_sticky_posts' => 1,
    'orderby' => 'post_date',
    'order' => 'DESC',
  );

  $my_query = new WP_Query($args);

  if ($my_query->have_posts()) {
    $output = "<ul>";
    while ($my_query->have_posts()) : $my_query->the_post();

      $id = get_the_ID();
      $essay = get_post_custom_values("essay", $id);

      $output .= "<li>";
      $output .= "<a href='" . get_permalink($id) . "'>" . get_the_title() . "</a>";

      // Safely handle the $essay variable
      if (!empty($essay) && isset($essay[0])) {
        $output .= "<br />" . $essay[0]; // Display essay content if available
      } else {
        $output .= "<br />No essay available."; // Optional fallback message
      }

      $output .= "</li>";
    endwhile;
    $output .= "</ul>";

    // Reset post data to avoid conflicts with other queries
    wp_reset_postdata();

    return $output;
  }

  // Fallback if no posts are found
  return "<p>No projects available at the moment.</p>";
}


add_filter('the_content', __NAMESPACE__ . '\\wpdocs_remove_shortcode_from_index', 6);


// custom file rename function;
add_filter('mfrh_new_filename', __NAMESPACE__ . '\\my_filter_filename', 10, 3);
function my_filter_filename($new, $old, $post)
{
  return "lovis_boe__" . $new;
}


// disable emoji stuff
function disable_wp_emojicons()
{

  // all actions related to emojis
  remove_action('admin_print_styles', 'print_emoji_styles');
  remove_action('wp_head', 'print_emoji_detection_script', 7);
  remove_action('admin_print_scripts', 'print_emoji_detection_script');
  remove_action('wp_print_styles', 'print_emoji_styles');
  remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
  remove_filter('the_content_feed', 'wp_staticize_emoji');
  remove_filter('comment_text_rss', 'wp_staticize_emoji');


  // filter to remove TinyMCE emojis
  add_filter('tiny_mce_plugins', __NAMESPACE__ . '\\disable_emojicons_tinymce');
}

add_action('init', __NAMESPACE__ . '\\disable_wp_emojicons');

function disable_emojicons_tinymce($plugins)
{
  if (is_array($plugins)) {
    return array_diff($plugins, array('wpemoji'));
  } else {
    return array();
  }
}

add_filter('emoji_svg_url', '__return_false');

// remove generator
remove_action('wp_head', 'wp_generator');

// remove jquery migrate
//add_action('wp_default_scripts', function ($scripts) {
//  if (!empty($scripts->registered['jquery'])) {
//    $scripts->registered['jquery']->deps = array_diff($scripts->registered['jquery']->deps, array('jquery-migrate'));
//  }
//});

flush_rewrite_rules(false);

function handle_opt_in_submit() {
  if (!empty($_POST['website'])) {
    // Honeypot triggered, reject the form
    wp_send_json_error(['message' => 'Bitte fülle nur die erforderlichen Felder aus.']);
  }

  // Process form data (POST to Listmonk)
  $listmonk_url = 'https://listmonk.lovis.de/subscription/form';
  $postData = $_POST;

  $ch = curl_init($listmonk_url);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  $response = curl_exec($ch);
  $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  curl_close($ch);

  if ($httpCode === 200) {
    wp_send_json_success(['message' => 'Vielen Dank! Deine Anmeldung war erfolgreich.']);
  } else {
    wp_send_json_error(['message' => 'Es gab ein Problem beim Absenden des Formulars. Bitte versuche es später erneut.']);
  }
}
add_action('wp_ajax_opt_in_submit',  __NAMESPACE__ . '\\handle_opt_in_submit');
add_action('wp_ajax_nopriv_opt_in_submit', __NAMESPACE__ . '\\handle_opt_in_submit');


// Disable /users rest routes for non-admins
add_filter('rest_endpoints', function( $endpoints ) {
  if ( isset( $endpoints['/wp/v2/users'] ) ) {
    unset( $endpoints['/wp/v2/users'] );
  }
  if ( isset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] ) ) {
    unset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] );
  }
  return $endpoints;
});
