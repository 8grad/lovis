"use strict";

/**
 * @constructor
 */


function ThemeSwiper(element, options) {
  var site = {};
  var self = this;
  self.$el = jQuery(element);
  self.options = jQuery.extend({}, ThemeSwiper.DEFAULTS, options);

  self.$slideItems = null;
  self.$sliderContainer = null;
  self.$contentContainer = null;
  self.slider = null;

  function init() {

    /* set up targets */
    site.siteSlider = self;
    self.$sliderContainer = jQuery('.swiper-container', self.$el);

    var $slidee = self.$sliderContainer.find(".swiper-wrapper");
    var prevClass = '.swiper-button-prev';
    var nextClass = '.swiper-button-next';
    var pagesClass = '.swiper-pagination';
    var scrollbarClass = '.swiper-scrollbar';
    var isLoadingClass = 'isLoading';



    /**
     * progressive enhancement:
     * 'moves' an child images of <li> into background-image of it
     */

    function imagesAsBackground() {
      $slidee.find('li').each(function () {
        var $li = jQuery(this);
        var $img = jQuery(this).find('img');
        var $imgSrc = $img.attr('src');

        //show image as background of li element for same sizes;
        if (typeof $imgSrc !== "undefined") {
          $li.attr("style", "background-image: url(" + $imgSrc + "); ");
          $img.css({'opacity': 0});
        }
      });
    }

    /**
     * Helper
     * */
    function betterLazyLoader() {
      $slidee.find('li img').each(function () {
        var $img = jQuery(this);
        var $imgSrc = jQuery(this).attr('data-src');
        var img = new Image();

        img.onload = function () {
          // image is loaded...
          $img.removeClass(isLoadingClass);
          $img.next('.swiper-lazy-preloader').remove();
        };

        $img.addClass(isLoadingClass);
        $img.attr('src', $imgSrc);

        // The onload function will now trigger once it's loaded.
        img.src = $imgSrc;

      });

      imagesAsBackground();
    }

    var mySwiper = new Swiper(self.$sliderContainer, { // jshint ignore:line
      // Autoplay
      autoplay: 20000,
      speed: 1000,
      autoplayDisableOnInteraction: true,

      //Loop ~ infinite
      loop: false,

      // Scrollbar
      scrollbar: scrollbarClass,
      scrollbarDraggable: true,
      scrollbarHide: false,
      scrollbarSnapOnRelease: true,

      centeredSlides: false,
      grabCursor: true,

      // Pagination
      pagination: pagesClass,
      paginationClickable: true,

      // Keyboard
      keyboardControl: true,
      nextButton: nextClass,
      prevButton: prevClass,

      // Mousewheel
      mousewheelControl: true,

      // Hash Navigation
      //hashnav: true,
      //hashnavWatchState: true,

      // No Fixed Position
      freeMode: false,

      onImagesReady: betterLazyLoader(),

      // Zoom
      zoom: true,

      slidesPerView: 'auto',
      spaceBetween: 0
    });

  }


  init();

}

ThemeSwiper.DEFAULTS = {};

ThemeSwiper.AutoInit = function () {
  jQuery('body').each(function () {
    var el = jQuery(this);
    var obj = el.data('theme.touch.slider');
    var data = el.data();

    if (!obj) {
      el.data('theme.touch.slider', (obj = new ThemeSwiper(this, data)));
    }

  });
};

