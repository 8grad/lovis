"use strict";

var site = {};

function Core() {
  var self = this;

  function init() {
    site.pluginWatcher = new PluginWatcher(); // jshint ignore:line
    self.update();
  }

  self.update = function () {
    setTimeout(site.pluginWatcher.checkPlugins, 500);
  };
  init();
}
