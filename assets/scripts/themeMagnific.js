"use strict";

/**
 * @constructor
 */

function ThemeMagnific(element, options) {
  var site = {};
  var self = this;
  self.$el = jQuery(element);
  self.options = jQuery.extend({}, ThemeMagnific.DEFAULTS, options);

  self.$menu = null;
  self.menuIsOpen = false;

  function init() {
    site.siteMenu = self;
    self.$video = jQuery('a[href*="youtube"], a[href*="vimeo"], a[href*=".mov"]', self.$el);
    self.$image = jQuery('.popup', self.$el);

    self.$video.magnificPopup({
      disableOn: 700,
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,

      fixedContentPos: false
    });

    self.$image.magnificPopup({
      type: 'image',
      closeOnContentClick: true,
      mainClass: 'mfp-img-mobile',
      image: {
        verticalFit: true
      }
    });
  }

  init();
}

ThemeMagnific.DEFAULTS = {};

ThemeMagnific.AutoInit = function () {
  jQuery('body').each(function () {
    var el = jQuery(this);
    var obj = el.data('theme.magnific');
    var data = el.data();

    if (!obj) {
      el.data('theme.magnific', (obj = new ThemeMagnific(this, data)));
    }
  });
};
