"use strict";

function PluginWatcher() {
  var self = this;

  self.checkPlugins = function () {
    /* the plugins */
    ThemeMenu.AutoInit();     // jshint ignore:line
    ThemeSwiper.AutoInit();   // jshint ignore:line
    ThemeMagnific.AutoInit(); // jshint ignore:line
  };
}
