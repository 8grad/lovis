"use strict";

/**
 * @constructor
 */

function ThemeMenu(element, options) {
  var site = {};
  var self = this;
  self.$el = jQuery(element);
  self.options = jQuery.extend({}, ThemeMenu.DEFAULTS, options);

  self.$menu = null;
  self.menuIsOpen = false;

  function init() {
    site.siteMenu = self;
    self.$menu = jQuery('.menu-link', self.$el);
    self.$panel = jQuery('#menu', self.$el);

    // init menu
    self.$menu.bigSlide({
      "speed": 450,
      "menuWidth": "280px",
      "easyClose": true,
      'beforeOpen': function () {
        self.$panel.addClass('visible');
        self.menuIsOpen = false;
      },
      'afterOpen': function () {
        self.menuIsOpen = true;
      },
      'afterClose': function () {
        self.$panel.removeClass('visible');
      }
    });

  }

  init();
}

ThemeMenu.DEFAULTS = {};

ThemeMenu.AutoInit = function () {
  jQuery('body').each(function () {
    var el = jQuery(this);
    var obj = el.data('theme.menu');
    var data = el.data();

    if (!obj) {
      el.data('theme.menu', (obj = new ThemeMenu(this, data)));
    }
  });
};
