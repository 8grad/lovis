��    '      T  5   �      `     a  
   w     �     �  	   �     �     �     �     �     �     �     �  	             !  	   8     B     G     N  	   [     e     �     �     �  ;   �     �     
  �   (     �     �  
   �     �  	   �     �               #  
   &  �  1     �     	     	     3	     ?	     K	  
   \	     g	     �	     �	     �	     �	  	   �	     �	  $   �	     �	     �	     �	     
     
  %   '
  
   M
  
   X
     c
  C   z
  7   �
  +   �
  h   "     �     �     �     �     �     �  *        ?     T     Y                                           $                                    "                                      !   
                   '   &           	           #         %    &larr; Older comments All Events Comments are closed. Construction Events Continued Cruises Overview Edit Error locating %s for inclusion Events Footer From In LOVIS 2.0 Newer comments &rarr; Next post in category: Not Found Page Pages: Parent event Photo ©  Previous post in category: Primary Primary Navigation Search Results for %s Sorry, but the page you were trying to view does not exist. Sorry, no events were found. Sorry, no results were found. You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience. create event create new event edit event events new event no event found no event in trash search events to view event Project-Id-Version: LOVIS 2.0
POT-Creation-Date: 2017-10-10 16:27+0200
PO-Revision-Date: 2017-10-10 16:29+0200
Last-Translator: Marc W. <info@marcwright.de>
Language-Team: Marc W. <info@marcwright.de>
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.5
X-Poedit-Basepath: ..
X-Poedit-WPHeader: style.css
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Ältere Kommentare Alle Termine Kommentarfunktion geschlossen. Bau-Termine Fortsetzung Törn-Übersicht Bearbeiten Fehler bei der Erfassung von %s Termine Footer Von In LOVIS 2.0 Neuere Kommentare &rarr; Nächster Termin in dieser Kategorie Nicht gefunden Seite Seiten: Übergeordnete Veranstaltung Foto © Vorheriger Termin in dieser Kategorie Hauptmenü Hauptmenü Suchergebnisse für %s Es tut uns leid, aber die von Ihnen gesuchte Seite existiert nicht. Leider wurden keine ähnlichen Veranstaltungen gefunden Es wurden leider keine Ergebnisse gefunden. Ihr Browser ist nicht mehr aktuell. Bitte aktualisieren Sie Ihren Browser, um die Nutzung zu verbessern. Veranstaltung anlegen	 Neue Veranstaltung erstellen Veranstaltung bearbeiten Bau-Termine Neue Veranstaltung Keine Veranstaltung gefunden Keine Veranstaltung im Papierkorb gefunden Veranstaltung suchen nach Veranstaltung anzeigen 