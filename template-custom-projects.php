<?php
/**
 * Template Name: Projects on Lovis
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

<hr />

<?php echo Roots\Sage\extras\projectsOnLovis(); ?>
