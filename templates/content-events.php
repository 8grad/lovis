<?php
$classes = array(
  \Roots\Sage\Extras\checkLegacyOfDate(get_the_ID()) )
?>

<article <?php post_class($classes) ?>>
  <header>
    <?php echo get_the_category_list(); ?>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php get_template_part('templates/entry-meta-events'); ?>
  </header>
  <div class="entry-summary">
    <?php the_content(); ?>
  </div>
</article>

<hr/>
<br/>
<br/>
