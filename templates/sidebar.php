<nav class="main-navigation">
  <?php dynamic_sidebar('sidebar-primary'); ?>
</nav>

<!-- will be toggled -->
<nav id="menu" class="mobile-navigation">
  <?php dynamic_sidebar('sidebar-primary'); ?>
</nav>



<section class="events">
  <?php Roots\Sage\Extras\get_the_events(); ?>
</section>
