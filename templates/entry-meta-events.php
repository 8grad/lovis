<?php

$theID = get_the_ID();
$subtitle = get_post_custom_values("untertitel", $theID);

$dateFrom = get_post_custom_values("von", $theID);
$dateTo = get_post_custom_values("bis", $theID);

$timeFrom = get_post_custom_values("von-zeit", $theID);
$timeTo = get_post_custom_values("bis-zeit", $theID);

$formatter = new IntlDateFormatter(
  'de_DE', // Locale (e.g., German)
  IntlDateFormatter::FULL, // Full representation of the day (e.g., Monday)
  IntlDateFormatter::NONE // No time representation
);
$formatter->setPattern('EEEE, d. MMMM yyyy'); // Custom pattern to match the desired format

$niceDateFrom = $formatter->format(new DateTime($dateFrom[0]));
$niceDateTo = $formatter->format(new DateTime($dateTo[0]));

$start = get_post_custom_values("start", $theID);
$end = get_post_custom_values("end", $theID);

$postDate = (new DateTime($dateFrom[0]))->format('Y-m-d');

?>


<time class="updated" datetime="<?php echo $postdate ?>">
  <h4><?php
    if (!empty($subtitle) && isset($subtitle[0])) {
      echo $subtitle[0];
    } else {
      echo ''; // Fallback to an empty string if $subtitle is null or invalid.
    }
    ?>
  </h4>
  <p><?php echo $niceDateFrom;

    // check begin time
    if (isset($timeFrom[0]) && $timeFrom[0] != '') {
      echo ", <b>(" . $timeFrom[0] . ")</b>";
    }
    echo "&mdash;" . $niceDateTo;
    // check end time
    if (isset($timeTo[0]) && $timeTo[0] != '') {
      echo ", <b>(" . $timeTo[0] . ")</b>";
    }
    ?><br/>
    <?php
    if (isset($end[0]) && $end[0] != '' && isset($start[0]) && $start[0] != '') {
      echo __('From', 'sage') . " " . $start[0] . " " . __('to', 'sage') . " " . $end[0];
    } else if (isset($start[0]) && $start[0] != '') {
      echo __('In', 'sage') . " " . $start[0];
    } else {
      //
    }
    ?>
  </p>
</time>
