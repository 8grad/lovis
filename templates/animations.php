<div class="animation__wrapper">
  <div class="animation__frame">
    <div class="animation__bird">
      <div class="animation__bird--body1"></div>
      <div class="animation__bird--body2"></div>
      <div class="animation__bird--wing-l"></div>
      <div class="animation__bird--wing-r"></div>
    </div>
  </div>

</div>
