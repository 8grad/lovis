<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <header>
      <?php echo get_the_category_list(); ?>
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <?php get_template_part('templates/entry-meta-events'); ?>
    </header>

    <div class="entry-content">
      <?php the_content(); ?>
    </div>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>


<nav class="navigation posts-navigation" role="navigation">
  <div class="nav-links">
    <div class="nav-previous">
      <?php previous_post_link('%link', __('Previous post in category:', 'sage'), $in_same_term = true); ?>
    </div>

    <div class="nav-next">
      <?php next_post_link('%link', __('Next post in category:', 'sage'), $in_same_term = true); ?>
    </div>
  </div>
</nav>
